var express = require("express");
var app = express();

app.get("/", function(req, res) {

        //Status
        res.status(202);
        //MIME type
        res.type("text/plain");
        res.send("The current time is " + (new Date()));
}
);

app.get("/hello", function(req, res) {

        //Status
        res.status(202);
        //MIME type
        res.type("text/plain");
        res.send("HELLO!");
    }
);

//3000 - port number
app.listen(4000, function(){
console.info("Application started on port 4000");
});

